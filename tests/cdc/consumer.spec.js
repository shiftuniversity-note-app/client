const assert = require('assert');
const { Pact } = require('@pact-foundation/pact');
const { fetchNotes, postNote } = require('../../src/api');
const { like } = require('check-types');

const baseURL = 'http://localhost:5000';

describe('Pact with note API', () => {
	const provider = new Pact({
		port: 5000,
		consumer: 'note-app-frontend',
		provider: 'note-app-backend',
		pactfileWriteMode: 'overwrite',
	});

	before(() => provider.setup());
	after(() => provider.finalize());

	describe('When a GET request is sent to the Note API', () => {
		const notes = [
			{
				_id: '1',
				note: 'lorem ipsum',
				_v: 0,
			},
			{
				_id: '2',
				note: 'lorem hamza',
				_v: 0,
			},
			{
				_id: '3',
				note: 'lorem abbas',
				_v: 0,
			},
		];
		before(async () => {
			return provider.addInteraction({
				state: 'Request to see all notes',
				uponReceiving: 'A request to get a note',
				withRequest: {
					path: '/notes',
					method: 'GET',
				},
				willRespondWith: {
					status: 200,
					body: { notes },
				},
			});
		});

		it('Will receive the list of notes', async () => {
			fetchNotes(baseURL)
				.then((res) => {
					assert.ok(res.status);
				})
				.catch((err) => {
					console.error(err);
				});
		});
	});

	describe('When a POST request is sent to the Note API', () => {
		const postData = {
			note: 'lorem ipsum',
		};

		before(async () => {
			return provider.addInteraction({
				state: 'Request to see posted note',
				uponReceiving: 'A request to get a posted note',
				withRequest: {
					path: '/notes',
					method: 'POST',
					body: {
						note: 'lorem ipsum',
					},
				},
				willRespondWith: {
					status: 201,
					body: {
						_id: '1',
						note: 'lorem ipsum',
						_v: 0,
					},
				},
			});
		});

		it('Will receive the posted note', async () => {
			postNote(baseURL, postData)
				.then((res) => {
					assert.ok(res.status);
				})
				.catch((err) => {
					console.error(err);
				});
		});
	});
});
