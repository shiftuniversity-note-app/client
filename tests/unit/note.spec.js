import { shallowMount } from '@vue/test-utils';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import Note from '@/components/Note.vue';

import { fetchNotes, postNote } from '../../src/api/index';

const mock = new MockAdapter(axios);

const { ENV } = process.env;

const apiURL =
	ENV === 'local' ? 'http://localhost:5000/' : 'http://localhost:5000/';

const getData = [
	{
		_id: '1',
		note: 'abbas',
		__v: 0,
	},
	{
		_id: '2',
		note: 'hamza',
		__v: 0,
	},
	{
		_id: '3',
		note: 'cabbar',
		__v: 0,
	},
];

const postData = {
	note: 'abbas',
};

const recivedData = {
	_id: '1',
	note: 'abbas',
	__v: 0,
};

const propsData = {
	title: 'Note App',
};

let wrapper = shallowMount(Note, {
	propsData,
});

describe('Note component tests', () => {
	describe('Mount test', () => {
		it('Is it mount', () => {
			expect(wrapper.exists()).toBeTruthy();
		});
	});

	describe('Note component title tests', () => {
		it('Is title rendered', () => {
			expect(wrapper.find('#title').exists()).toBeTruthy();
		});

		it('Props work properly', () => {
			expect(wrapper.find('#title').text()).toBe(propsData.title);
		});
	});

	describe('Note component textbox tests', () => {
		it('Is textbox rendered', () => {
			expect(wrapper.find('#textbox').exists()).toBeTruthy();
		});
	});

	describe('Note component list tests', () => {
		it('Is list rendered', () => {
			expect(wrapper.find('#list').exists()).toBeTruthy();
		});
	});

	describe('Fetch api function test', () => {
		afterAll(() => mock.restore());
		beforeEach(() => mock.reset());

		it('Fetch notes', async () => {
			mock.onGet(apiURL + '/notes').reply(200, getData);

			const res = await fetchNotes(apiURL);

			expect(res.status).toBe(200);
		});

		it('Post note', async () => {
			mock.onPost(apiURL + '/notes').reply(201, recivedData);

			const res = await postNote(apiURL, postData);

			expect(res.status).toBe(201);
			expect(res.data).toMatchObject(recivedData);
		});
	});
});
